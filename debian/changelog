r-cran-rpact (4.1.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Wed, 02 Oct 2024 16:25:51 +0900

r-cran-rpact (4.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Tue, 16 Jul 2024 09:07:23 +0900

r-cran-rpact (3.5.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 01 Feb 2024 17:01:55 +0100

r-cran-rpact (3.4.0-1) unstable; urgency=medium

  * New upstream version
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 05 Jul 2023 11:21:27 +0200

r-cran-rpact (3.3.4-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 26 Feb 2023 12:49:50 +0100

r-cran-rpact (3.3.2-1) unstable; urgency=medium

  * New upstream version
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 18 Nov 2022 11:46:10 +0100

r-cran-rpact (3.3.1-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Repository, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Thu, 15 Sep 2022 16:40:30 +0200

r-cran-rpact (3.3.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 24 Jun 2022 11:13:12 +0200

r-cran-rpact (3.2.3-1) unstable; urgency=medium

  * New upstream version
  * Adjust Test-Depends to Suggests inside DESCRIPTION

 -- Andreas Tille <tille@debian.org>  Fri, 11 Mar 2022 10:21:13 +0100

r-cran-rpact (3.2.1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.2.1
  * Run cme

 -- Nilesh Patra <nilesh@debian.org>  Mon, 10 Jan 2022 15:20:15 +0530

r-cran-rpact (3.2.0-2) unstable; urgency=medium

  * Team Upload.
  * Ignore more tests on i386

 -- Nilesh Patra <nilesh@debian.org>  Fri, 31 Dec 2021 01:41:07 +0530

r-cran-rpact (3.2.0-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.2.0
  * refresh patch

 -- Nilesh Patra <nilesh@debian.org>  Tue, 28 Dec 2021 18:30:59 +0530

r-cran-rpact (3.1.1-1) unstable; urgency=medium

  * Team Upload.
  [ Andreas Tille ]
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Disable reprotest

  [ Nilesh Patra ]
  * d/p/do-not-use-fancy-quotes.patch: Fix autopkgtests

 -- Nilesh Patra <nilesh@debian.org>  Tue, 21 Sep 2021 16:23:58 +0530

r-cran-rpact (3.0.3-2) unstable; urgency=high

  * Drop some failing test for i386

 -- Andreas Tille <tille@debian.org>  Wed, 10 Feb 2021 11:19:28 +0100

r-cran-rpact (3.0.3-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * Test-Depends: r-cran-mnormt

 -- Andreas Tille <tille@debian.org>  Sat, 16 Jan 2021 17:00:15 +0100

r-cran-rpact (3.0.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 30 Sep 2020 11:24:24 +0200

r-cran-rpact (3.0.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 09 Sep 2020 08:57:25 +0200

r-cran-rpact (2.0.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Mon, 06 Jan 2020 07:54:05 +0100

r-cran-rpact (2.0.5-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 12 Nov 2019 14:31:13 +0100

r-cran-rpact (2.0.4-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1

 -- Andreas Tille <tille@debian.org>  Mon, 07 Oct 2019 13:34:23 +0200

r-cran-rpact (2.0.3-1) unstable; urgency=medium

  * New upstream version
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Bug-Database.

 -- Andreas Tille <tille@debian.org>  Mon, 23 Sep 2019 15:30:19 +0200

r-cran-rpact (2.0.2-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Thu, 01 Aug 2019 08:47:25 +0200

r-cran-rpact (2.0.1-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Wed, 10 Jul 2019 22:17:49 +0200

r-cran-rpact (1.0.0-1) unstable; urgency=medium

  * Initial release (closes: #919403)

 -- Andreas Tille <tille@debian.org>  Tue, 15 Jan 2019 17:24:40 +0100
